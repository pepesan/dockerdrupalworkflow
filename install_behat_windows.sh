#!/bin/bash
source .env
set -eux

docker-compose exec app composer require --dev -d /var/www "behat/behat" "behat/mink" "behat/mink-extension" "behat/mink-browserkit-driver"
