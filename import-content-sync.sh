#!/bin/bash
source .env
set -eux
docker-compose exec -u www-data app drush -y content-sync-import