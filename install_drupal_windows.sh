#!/bin/bash
source .env
set -eux

docker-compose exec -u root app chown -R www-data:www-data //var/www


docker-compose exec -u www-data app composer require --dev -d //var/www "drush/drush:$DRUSH_VERSION" drupal/drupal-extension "drupal/core-dev:$DRUPAL_VERSION" composer/installers cweagans/composer-patches webflo/drupal-finder webmozart/path-util

if [ "$DRUPAL_PROFILE" == "oe_profile" ]
then
docker-compose exec -u www-data   app composer require --dev -d //var/www ec-europa/toolkit openeuropa/composer-artifacts

docker-compose exec -u www-data   app composer require -d //var/www openeuropa/oe_authentication openeuropa/oe_corporate_blocks openeuropa/oe_media openeuropa/oe_paragraphs openeuropa/oe_profile  openeuropa/oe_webtools

docker-compose exec -u www-data   app  mkdir -p //var/www/web/themes/contrib/oe_theme/components
fi

docker-compose exec -u www-data -e DRUPAL_THEME=$DRUPAL_THEME_FULL app composer require -d //var/www $DRUPAL_THEME_FULL

docker-compose exec -u www-data app composer require  -d //var/www drupal/redis predis/predis
docker-compose exec -u www-data app composer update -d //var/www

docker-compose exec -u www-data -e ADMIN_PASSWORD=$ADMIN_PASSWORD -e PROJECT_NAME=$PROJECT_NAME -e DRUPAL_PROFILE=$DRUPAL_PROFILE app drush si $DRUPAL_PROFILE --locale=es --account-name=$ADMIN_ACCOUNT --account-pass=$ADMIN_PASSWORD --account-mail=$ADMIN_EMAIL --site-name=$PROJECT_NAME --db-url=mysql://$MYSQL_USER:$MYSQL_PASSWORD@$DB_HOST:3306/$MYSQL_DATABASE -y --debug

docker-compose exec -u www-data app drush -y en ckeditor

docker-compose exec -u www-data app composer require drupal/drush_language -d //var/www
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush -y en drush_language
docker-compose exec -u www-data app drush -y language-add es
docker-compose exec -u www-data app drush -y language-add en
docker-compose exec -u www-data app drush -y language-default $DEFAULT_LANGUAGE

docker-compose exec -u www-data app drush -y en redis


docker-compose exec -u www-data app drush -y en media media_library

if [ "$DRUPAL_PROFILE" == "oe_profile" ]
then
  docker-compose exec -u www-data app drush -y en oe_media oe_webtools oe_paragraphs oe_media oe_profile oe_corporate_blocks
fi
if [ "$DRUPAL_THEME_SHORT" == "oe_theme" ]
then
  docker-compose exec -u www-data app npm install --prefix themes/contrib/oe_theme
  docker-compose exec -u www-data \
  app npm run build --prefix themes/contrib/oe_theme
fi

docker-compose exec -u www-data app drush -y en layout_builder  layout_discovery

if [ "$DRUPAL_THEME_SHORT" == "radix" ]
then

docker-compose exec -u www-data app drush -y en components
docker-compose exec app npm install -g npm
docker-compose exec -u www-data app npm install --prefix themes/contrib/radix
fi
if [ "$DRUPAL_THEME_FULL" == "drupal/bootstrap_barrio" ]
then
  ./create_subtheme_docker_compose_windows.sh
fi

docker-compose exec -u www-data app composer require -d //var/www drupal/gutenberg
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush -y en gutenberg

docker-compose exec -u www-data -e DRUPAL_THEME=$DRUPAL_THEME_SHORT app drush -y theme:enable $DRUPAL_THEME_SHORT
docker-compose exec -u www-data -e DRUPAL_THEME=$DRUPAL_THEME_SHORT app drush  -y config:set system.theme default $DRUPAL_THEME_SHORT


docker-compose exec -u www-data app composer require -d //var/www drupal/search_api_solr drupal/facets drupal/facets_rest drupal/search_api_autocomplete
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush -y pmu page_cache big_pipe search

docker-compose exec -u www-data app drush  -y en search_api_solr facets facets_rest search_api_autocomplete search_api_solr_admin
docker-compose exec solr solr create_core -c drupal -d //opt/solr/server/solr/drupal

docker-compose exec -u www-data app composer require -d //var/www drupal/matomo
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush -y en matomo

cp ./install-matomo.sh ./volumes-dev/matomo-tmp
chmod 755 ./volumes-dev/matomo-tmp/install-matomo.sh
# execute install-matomo
docker-compose exec -u www-data matomo-app //tmp/install-matomo.sh
# delete install-matomo
rm ./volumes-dev/matomo-tmp/install-matomo.sh





docker-compose exec -u www-data app composer require -d //var/www drupal/adv_varnish

docker-compose exec -u www-data app composer update -d //var/www

docker-compose exec -u www-data app drush -y en adv_varnish

docker-compose exec -u www-data app composer require -d //var/www drupal/pathauto drupal/ds  drupal/metatag drupal/workflow drupal/menu_breadcrumb
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush -y en pathauto metatag metatag_views workflows content_moderation content_translation menu_breadcrumb

docker-compose exec -u www-data   app composer require -d //var/www drupal/allowed_formats drupal/entity_browser drupal/inline_entity_form drupal/paragraphs drupal/smart_trim drupal/ui_patterns
docker-compose exec -u www-data app composer update -d //var/www
docker-compose exec -u www-data app drush  -y en path_alias responsive_image allowed_formats entity_browser entity_browser_entity_form paragraphs smart_trim ui_patterns ui_patterns_library ui_patterns_ds ui_patterns_views ui_patterns_layouts

#docker-compose exec -u www-data app composer require -d //var/www drupal/openapi drupal/openapi_ui_redoc drupal/openapi_ui drupal/openapi_rest drupal/restui drupal/entity drupal/simple_oauth
#docker-compose exec -u www-data app composer update -d //var/www
#docker-compose exec -u www-data app drush  -y en openapi rest restui hal openapi_ui_redoc openapi_ui openapi_rest jsonapi entity simple_oauth

docker-compose exec app composer require -d //var/www drupal/devel

docker-compose exec -u www-data app drush  -y   en devel devel_generate

docker-compose exec app composer require -d /var/www drupal/admin_toolbar

docker-compose exec -u www-data app drush  -y en admin_toolbar admin_toolbar_tools admin_toolbar_links_access_filter admin_toolbar_search

./install_behat_windows.sh
./import_config_dev_windows.sh
./check_drupal_dev_windows.sh
./backup-database_windows.sh

docker-compose exec -u root app chown -R $DUID:$DGID /var/www/web
chown -R $DUID:$DGID ./volumes-$ENV_STAGE/app-tmp
sudo chmod -R 777 ./volumes-$ENV_STAGE/app-tmp
docker-compose exec app chmod 777 /var/www/web
chmod 775 ./drupal_etc/settings.php
