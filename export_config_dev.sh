#!/bin/bash
source .env 
set -eux
sudo chmod -R 777  ./volumes-$ENV_STAGE/app-tmp/import/
#import all configs
docker-compose exec -u www-data \
  app drush  -y \
  config-export --destination /tmp/import
docker cp app-$ENV_STAGE:/tmp/import/* ./drupal_etc/drupal_sync_all
for d in $(find ./drupal_etc/drupal_sync_all -maxdepth 2 -type d); do for file in $d/*; do if [ -e $file ]; then echo $file; fi; done; done;