#!/bin/bash
# Script to quickly create sub-theme.
set -eux
source .env
echo '
+------------------------------------------------------------------------+
| With this script you could quickly create barrio sub-theme             |
| In order to use this:                                                  |
| - bootstrap_barrio theme (this folder) should be in the contrib folder |
+------------------------------------------------------------------------+
'

if [[ ! -e $BASE_THEME_PATH/custom ]]; then
    mkdir -p $BASE_THEME_PATH/custom ;
    mkdir -p $BASE_THEME_PATH/custom/$CUSTOM_BARRIO ;
fi
#cp -r $BASE_THEME_PATH/contrib/bootstrap_barrio/subtheme $BASE_THEME_PATH/custom/$CUSTOM_BARRIO
for file in ./themes/contrib/bootstrap_barrio/subtheme/*;
do
  if [[ -d $file ]]; then
    mkdir -p themes/custom/$CUSTOM_BARRIO/$file;
    cp -r $file themes/custom/$CUSTOM_BARRIO;
  else
    cp $file themes/custom/$CUSTOM_BARRIO;
  fi
done
for file in $BASE_THEME_PATH/custom/$CUSTOM_BARRIO/*bootstrap_barrio_subtheme.*; do mv $file ${file//bootstrap_barrio_subtheme/$CUSTOM_BARRIO}; done
for file in $BASE_THEME_PATH/custom/$CUSTOM_BARRIO/config/*/*bootstrap_barrio_subtheme.*; do mv $file ${file//bootstrap_barrio_subtheme/$CUSTOM_BARRIO}; done
mv $BASE_THEME_PATH/custom/$CUSTOM_BARRIO/{_,}$CUSTOM_BARRIO.theme
#grep -Rl bootstrap_barrio_subtheme $BASE_THEME_PATH/custom/$CUSTOM_BARRIO/|xargs sed -i '' -e "s/bootstrap_barrio_subtheme/$CUSTOM_BARRIO/"
sed -i -e "s/Bootstrap Barrio Subtheme/$CUSTOM_BARRIO_NAME/" $BASE_THEME_PATH/custom/$CUSTOM_BARRIO/$CUSTOM_BARRIO.info.yml
echo "# Check the themes/custom folder for your new sub-theme."