#!/bin/bash
source .env
set -eux
MODULE_NAME=$1

docker-compose exec -u www-data \
	app composer \
	require \
	-d /var/www \
	drupal/"$MODULE_NAME"
# Enable module
docker-compose exec -u www-data app drush -y en "$MODULE_NAME"

