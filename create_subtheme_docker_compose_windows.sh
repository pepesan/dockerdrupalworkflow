#!/bin/bash
source .env
set -eux
# Script to quickly create sub-theme on docker
cp create_subtheme_windows.sh ./drupal/web
cp .env ./drupal/web
docker-compose exec app chmod +x //var/www/web/create_subtheme_windows.sh
docker-compose exec app mkdir -p //var/www/web/themes/custom
docker-compose exec app mkdir -p //var/www/web/themes/custom/$CUSTOM_BARRIO
docker-compose exec app ./create_subtheme_windows.sh
docker-compose exec -u www-data app rm //var/www/web/create_subtheme_windows.sh
docker-compose exec -u www-data app rm //var/www/web/.env
