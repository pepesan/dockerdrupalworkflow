#!/bin/bash
source .env
set -eux
# Backup database previous to update
./backup-database.sh
docker-compose down
docker image prune -y
docker-compose exec -u root app chown -R $DUID:$DGID /var/www
sudo chown -R $DUID:$DGID ./volumes-$ENV_STAGE/app-tmp
sudo chmod -R 777 ./volumes-$ENV_STAGE/app-tmp
sudo docker-compose exec app chmod 777 /var/www
sudo chmod 775 ./drupal_etc/settings-$ENV_STAGE.php
docker-compose up -d
./backup-database.sh
