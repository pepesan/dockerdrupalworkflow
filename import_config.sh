#!/bin/bash
source .env
set -eux
#import all configs
docker cp ./drupal_etc/drupal_sync/* app-$ENV_STAGE:/tmp/import
docker-compose exec -u www-data \
  app drush  -y \
  config-import --partial --source /tmp/import