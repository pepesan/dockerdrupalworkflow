# Pending tasks
* ansible scripts 
  * https://docs.ansible.com/ansible/latest/collections/community/general/docker_compose_module.html
  * https://docs.ansible.com/ansible/latest/collections/community/general/docker_network_module.html
  * https://docs.ansible.com/ansible/latest/collections/community/general/docker_image_module.html
  * https://docs.ansible.com/ansible/latest/collections/community/general/docker_container_module.html
  * https://docs.ansible.com/ansible/latest/collections/community/general/docker_stack_module.html
* Drupal 9 upgrade: https://dev.acquia.com/drupal9/deprecation_status and https://www.youtube.com/watch?v=3diyagvUBq4
* Check drupal helm chart https://github.com/drupalwxt/helm-drupal
* Check nexus helm charts https://github.com/Oteemo/charts
* check drupal rector to patch drupal modules to upgrade 
* Check Content Type Rest tutorial https://www.conasa.es/blog/crear-una-api-rest-drupal-8/
* Review JSON:API security https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/security-considerations
* Check the bootstrap barrio subtheme guide: https://www.webwash.net/getting-started-with-bootstrap-4-using-barrio-in-drupal-8/
* Check Gridstack for layout builder/discovery: https://www.drupal.org/project/gridstack
# Done tasks
* check dynamic page cache module https://www.drupal.org/docs/8/core/modules/dynamic-page-cache/overview
* Review the prod environment deployment:
** permission on folders: public, private etc
** docker images
* Check content sync with https://www.drupal.org/project/content_sync https://www.drupal.org/docs/8/modules/content-synchronization/drush-commands
* Check deploy module: https://www.drupal.org/docs/8/modules/deploy/deploy-for-drupal-8 (done, not updated)
* Sync Drupal configuration script
* Check OE theme modules and blocks
* Check solr configuration in docker compose
* Mount solr drupal core in docker compose  
* Check the new installation script with the profile and theme environment variables
* Check the solr configuration with several languages
* Add xdebug extension to the app container
* Check script to update drupal 
* Check default official modules
* Check swagger/Redoc drupal api integration
* Check ui_patterns