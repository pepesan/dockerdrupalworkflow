#!/bin/bash
source .env
set -eux
#docker-compose exec app pwd
#docker-compose exec app composer -d../ update
#docker-compose exec app composer -d../ install

docker-compose exec -u root app chown -R $DUID:$DGID /var/www
#echo "instalando en mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${DB_HOST}/${MYSQL_DATABASE}"
echo "instalando en mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${DB_HOST}/${MYSQL_DATABASE}"

# dev requirements

docker-compose exec  \
  app composer \
  require --dev \
  -d /var/www \
  "drush/drush:$DRUSH_VERSION" drupal/drupal-extension "drupal/core-dev:$DRUPAL_VERSION" composer/installers cweagans/composer-patches webflo/drupal-finder webmozart/path-util
# Open Europe dependencies
if [ "$DRUPAL_PROFILE" == "oe_profile" ]
then
docker-compose exec \
  app composer \
  require --dev \
  -d /var/www \
  ec-europa/toolkit openeuropa/composer-artifacts


  docker-compose exec \
  app composer \
  require \
  -d /var/www \
  openeuropa/oe_authentication openeuropa/oe_corporate_blocks openeuropa/oe_media openeuropa/oe_paragraphs openeuropa/oe_profile  openeuropa/oe_webtools

  #Dependencies oe_theme
  docker-compose exec -u www-data \
  app  \
  mkdir -p /var/www/web/themes/contrib/oe_theme/components
fi
# Install theme indicated in .env file
docker-compose exec \
  -e DRUPAL_THEME=$DRUPAL_THEME_FULL \
  app composer \
  require \
  -d /var/www \
  $DRUPAL_THEME_FULL

#Dependencies installation: redis
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/redis predis/predis

docker-compose exec \
    -e DB_HOST=$DB_HOST \
    -e DB_USER=$MYSQL_USER \
    -e DB_PASSWORD_$MYSQL_PASSWORD \
    -e DB_NAME=$MYSQL_DATABASE \
    -e ADMIN_PASSWORD=$ADMIN_PASSWORD \
    -e PROJECT_NAME=$PROJECT_NAME \
    -e DRUPAL_PROFILE=$DRUPAL_PROFILE \
    app drush \
    si $DRUPAL_PROFILE \
    --locale=es \
    --account-name=$ADMIN_ACCOUNT \
    --account-pass=$ADMIN_PASSWORD \
    --account-mail=$ADMIN_EMAIL \
    --site-name=$PROJECT_NAME \
    -y \
    --debug
# CKeditor activation
docker-compose exec -u www-data \
  app drush  -y \
  en ckeditor
# Language Modules configuration
docker-compose exec \
  app \
  composer require drupal/drush_language:1.0-rc4 -d /var/www
docker-compose exec -u www-data \
  app drush  -y \
  en drush_language
docker-compose exec -u www-data \
  app drush  -y \
  language-add es
docker-compose exec -u www-data \
  app drush  -y \
  language-add en
docker-compose exec -u www-data \
  app drush  -y \
  language-default $DEFAULT_LANGUAGE

docker-compose exec -u www-data \
  app drush -y \
  en redis

# Activate Media and media library
docker-compose exec -u www-data \
  app drush -y \
  en media media_library
# Open Europe dependencies
if [ "$DRUPAL_PROFILE" == "oe_profile" ]
then
  # EU module Activation
  docker-compose exec -u www-data \
  app drush -y \
  en oe_media oe_webtools oe_paragraphs oe_media oe_profile oe_corporate_blocks

  # eu login auth (disabled by default for development)
  #docker-compose exec -u www-data \
  #  app drush -y \
  #  en oe_authentication


fi
if [ "$DRUPAL_THEME_SHORT" == "oe_theme" ]
then
  # TODO pending check oe_theme
  docker-compose exec -u www-data \
  app npm install --prefix themes/contrib/oe_theme
  docker-compose exec -u www-data \
  app npm run build --prefix themes/contrib/oe_theme
fi
# activate layout builder
docker-compose exec -u www-data \
  app drush -y \
  en layout_builder  layout_discovery
# Experimental functionality
#docker-compose exec -u www-data \
#  app drush -y \
#  en field_layout

if [ "$DRUPAL_THEME_SHORT" == "radix" ]
then
#Dependencies installation: radix
#docker-compose exec -u www-data \
#  app composer \
#  require \
#  -d /var/www \
#  drupal/components:"2.0.0-beta3 as 1.1.0" #required 2020-09-22 to d9 dep
docker-compose exec -u www-data \
  app drush -y \
  en components
docker-compose exec \
  app npm install -g npm
 docker-compose exec -u www-data \
  app npm install --prefix themes/contrib/radix
fi
if [ "$DRUPAL_THEME_FULL" == "drupal/bootstrap_barrio" ]
then
  ./create_subtheme_docker_compose.sh
fi
#install gutenberg
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/gutenberg
docker-compose exec -u www-data \
  app drush -y \
  en gutenberg
# enabling theme
docker-compose exec -u www-data \
  -e DRUPAL_THEME=$DRUPAL_THEME_SHORT \
  app drush -y \
  theme:enable $DRUPAL_THEME_SHORT
docker-compose exec -u www-data \
  -e DRUPAL_THEME=$DRUPAL_THEME_SHORT \
  app drush  -y \
  config:set system.theme default $DRUPAL_THEME_SHORT

#Solr and server by module
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/search_api_solr \
  drupal/facets drupal/facets_rest drupal/search_api_autocomplete \
  # drupal/search_api_spellcheck
# Disable Page cache and big_pipe for varnish and search for solr
docker-compose exec -u www-data \
  app drush  -y \
  pmu page_cache big_pipe search
# activating modules
docker-compose exec -u www-data \
  app drush  -y \
  en search_api_solr \
  facets facets_rest search_api_autocomplete search_api_solr_admin \
  #  search_api_spellcheck search_api_solr_defaults
# creating te solr core
docker-compose exec \
  solr \
  solr create_core -c drupal -d /opt/solr/server/solr/drupal

#took the config.zip y apply it
#export SOLR_VERSION=8.6.1
#sudo chmod 777 ./volumes/app-tmp
#cp config-drupal.zip ./volumes/app-tmp
#docker-compose exec -u www-data \
#  -e SOLR_VERSION=$SOLR_VERSION  \
#  app drush  -y \
#
# Matomo
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/matomo
docker-compose exec -u www-data \
  app drush  -y \
  en matomo
# [pending] Config de matomo

# copy install-matomo
sudo cp ./install-matomo.sh ./volumes-$ENV_STAGE/matomo-tmp
sudo chmod 755 ./volumes-$ENV_STAGE/matomo-tmp/install-matomo.sh
# execute install-matomo
docker-compose exec -u www-data \
  matomo-app /tmp/install-matomo.sh
# delete install-matomo
sudo rm ./volumes-$ENV_STAGE/matomo-tmp/install-matomo.sh




# Install varnish
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/adv_varnish


docker-compose exec -u www-data \
  app drush  -y \
  en adv_varnish

# oe staff some dependencies

#docker-compose exec -u www-data \
#  app composer \
#  require \
#  -d /var/www \
#  drupal/json_field drupal/geofield

# TODO pending revision
#docker-compose exec -u www-data \
#  app drush  -y \
#  en json_field drupal/geofield

# Install upgrade_status module for drupal 9 upgrade status
#docker-compose exec -u www-data \
#  app composer \
#  require --dev \
#  -d /var/www \
#  drupal/upgrade_status drupal/upgrade_rector
#docker-compose exec -u www-data \
#  app drush  -y \
#  en upgrade_status upgrade_rector

# Common modules
# Install common modules
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/pathauto drupal/ds  drupal/metatag drupal/workflow drupal/menu_breadcrumb
  # drupal/simplenews  drupal/module_filter
# Enable Common Modules
docker-compose exec -u www-data \
  app drush  -y \
  en pathauto metatag metatag_views workflows content_moderation content_translation menu_breadcrumb
  # simplenews module_filter
# Install entity, patterns, paragraphs, etc... modules
docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/allowed_formats drupal/entity_browser  drupal/paragraphs drupal/smart_trim drupal/ui_patterns \
  # drupal/inline_entity_form
# Enable entity, patterns, paragraphs, etc... modules
docker-compose exec -u www-data \
  app drush  -y \
  en \
  path_alias responsive_image allowed_formats entity_browser paragraphs smart_trim ui_patterns ui_patterns_library ui_patterns_ds ui_patterns_views ui_patterns_layouts \
  # entity_browser_entity_form

# API Modules
# Install modules
#docker-compose exec \
#  app composer \
#  require \
#  -d /var/www \
#  drupal/openapi:2.0-rc3  drupal/openapi_ui_redoc:1.0-rc3 drupal/openapi_ui:1.0-rc3 drupal/openapi_rest:2.0-rc2 drupal/schemadata:@dev drupal/restui drupal/entity drupal/simple_oauth
# Enable modules
#docker-compose exec -u www-data \
#  app drush  -y \
#  en \
#  openapi rest restui hal openapi_ui_redoc openapi_ui openapi_rest jsonapi entity simple_oauth

docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/devel
# Enable devel modules
docker-compose exec -u www-data \
  app drush  -y \
  en \
  devel devel_generate

docker-compose exec \
  app composer \
  require \
  -d /var/www \
  drupal/admin_toolbar
# Enable devel modules
docker-compose exec -u www-data \
  app drush  -y \
  en \
  admin_toolbar admin_toolbar_tools admin_toolbar_links_access_filter admin_toolbar_search
 #
#docker-compose exec app composer require kielabokkie/jsonapi-behat-extension --dev -d /var/www
#docker-compose exec app composer require imbo/behat-api-extension --dev -d /var/www

# Docker content sync
#sudo mkdir -p ./volumes/app-tmp/sync
#sudo chmod 777 ./volumes/app-tmp/sync
#docker-compose exec app chown www-data:www-data /tmp/sync
#docker-compose exec -u www-data \
#  app composer \
#  require -d /var/www \
#  drupal/content_sync:2.x-dev
#docker-compose exec \
#  -u www-data app \
#  drush -y en content_sync
./install_behat.sh
# Import config for development environment
./import_config_dev.sh
./check_drupal_dev.sh
./backup-database.sh

docker-compose exec -u root app chown -R $DUID:$DGID /var/www/web
sudo chown -R $DUID:$DGID ./volumes-$ENV_STAGE/app-tmp
sudo chmod -R 777 ./volumes-$ENV_STAGE/app-tmp
sudo docker-compose exec app chmod 777 /var/www/web
sudo chmod 775 ./drupal_etc/settings.php
