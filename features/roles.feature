@d8 @api
Feature: Roles
  Check Roles
  
  Scenario Outline: Test that roles are created

    Given I am not logged in
    When I visit "/user"

    Then I should see "Username"
    When I enter "admin" for "Username"
    And I enter "admin" for "Password"
    And I press the "Log in" button

    Then I should see the link "View"
    And I should see the link "Edit"

    When I am on "/admin/people/roles"
    Then I should see the text <role>

    Examples:
      | role |
      |  "Anonymous user"   |
      |  "Authenticated user"   |
      |  "Administrator"   |